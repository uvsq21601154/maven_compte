public class UnsignedNumberException extends Exception{
    public UnsignedNumberException(String s){
        super(s);
    }
}
