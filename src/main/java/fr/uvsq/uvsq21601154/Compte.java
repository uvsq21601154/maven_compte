public class Compte {
    private int solde;

    public Compte() {
        solde = 0;
    }

    public Compte(int solde_initial) throws UnsignedNumberException {
        if (solde_initial > 0)
            solde = solde_initial;
        else
            throw new UnsignedNumberException("Solde négatif non autorisé");
    }

    public void credit(int montant) throws UnsignedNumberException {
        if (montant > 0)
            solde += montant;
        else
            throw new UnsignedNumberException("Impossible de créditer un montant négatif");
    }

    public void debit(int montant) throws UnsignedNumberException, BalanceExceededException {
        if (montant > 0)
            if (montant < solde)
                solde -= montant;
            else
                throw new BalanceExceededException("Le montant débité dépasse le solde");
        else
            throw new UnsignedNumberException("Montant négatif non autorisé");
    }

    public void virement(Compte destination, int montant) throws UnsignedNumberException, BalanceExceededException {
        if (montant > 0)
            if (montant < solde) {
                this.debit(montant);
                destination.credit(montant);
            } else
                throw new BalanceExceededException("Le montant débité dépasse le solde");
        else
            throw new UnsignedNumberException("Montant négatif non autorisé");
    }

    public int getSolde() {
        return solde;
    }
}

