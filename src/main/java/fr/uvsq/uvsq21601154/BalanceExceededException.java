public class BalanceExceededException extends Exception{
    public BalanceExceededException(String s){
        super(s);
    }
}
